<?php
/** 
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
/** El nombre de tu base de datos de WordPress */
define('DB_NAME', 'wordpress');

/** Tu nombre de usuario de MySQL */
define('DB_USER', 'root');

/** Tu contraseña de MySQL */
define('DB_PASSWORD', '');

/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define('DB_HOST', 'localhost');

/** Codificación de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8mb4');

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'cORHX-;A !7mDeFB)XyiW!hJLHc%n;U4I~jlh5),Y.@}nlU]L&|f6*htss&2fGTR');
define('SECURE_AUTH_KEY', 'jv<vd,$uF 5<fxMX?`o|nWc|MoNC8-HM48$]]]L+{}F~6Y%H5NL<%g]0};#Uu:JQ');
define('LOGGED_IN_KEY', '0CO`M?jYa#n4dN4V4*c912Ndp3<r@ay56Y*Mz#eU4S>,QJ-t~Y1H$ucc,UAdD1!$');
define('NONCE_KEY', '?IAdgK)V/F8v?OS5$[y6~1XdwT_8y&R~!1?yiK{>+WiK6$!HX$cbU}HE#]^$je]t');
define('AUTH_SALT', '$[-yH[hX8G7mwh}]Y*6Uu=C$3t9Qn$D_.NM<^Q9_<nj`EZhrBXLZoVB.^D)r$0Bs');
define('SECURE_AUTH_SALT', '3N#khj)t0&M=v*/vY+K[-nr]vl)uU1?S*2xmMb]kxc):QF,:g}!1tjE=ksnDM5]!');
define('LOGGED_IN_SALT', 'DQCMSIkl$XeZ^m0.L4xzx0FJsH,YF*m8)hHUJ2M]sdNbrw+{(ajVT*/HH^hD6//v');
define('NONCE_SALT', '%s2paYL)8h.h.2!lwkJ<AOD@C|_f<#=8s_wQcylHXdMMU0pB|cg5F}-6ZqEtG|e0');

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix  = 'wp_';


/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

